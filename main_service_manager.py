#!/usr/bin/python2
from flask import jsonify
from flask import request

from classes.common.class_log import Log
from classes.common.class_yaml_config_parser import ConfigParser
from classes.processes.class_processes_manager import ProcessesManager
from classes.service.class_service import Service

# Read the configuration YAML file
objectConfigParser = ConfigParser('/data/processes/process_manager/config/config.yaml')
configDocumentHandler = objectConfigParser.getDocumentHandler()
# Create the log object
objectLog = Log(configDocumentHandler)
objectLog.logInit()
# Create the service object and get the app
objectService = Service(objectConfigParser, objectLog)
app = objectService.getApp()

@app.route(configDocumentHandler['generall']['service']['serviceName'], methods=['GET'])
def eventsListener():
    # Test the part of threading
    if request.args:
        processManagerObject = ProcessesManager(objectConfigParser, objectLog, request.args)
        processManagerObject.execute()
        #print(request.args)
    response = [
        {
            'status': 200,
            'title': u'Processes Manager Events Handler',
            'done': True
        }
    ]
    return jsonify({'response': response})

if __name__ == '__main__':
    try:
        # Start the service
        if app:
            hostName = configDocumentHandler['generall']['service']['host']
            hostPort = int(configDocumentHandler['generall']['service']['port'])
            app.run(host=hostName, port=hostPort, debug=False, threaded=True)
        else:
            objectLog.writeError('Service not successfully started')
            #print('Service not successfully started')
    except KeyboardInterrupt:
        pass
    finally:
        objectLog.writeError('Service stopped')
        #print('Service stopped')