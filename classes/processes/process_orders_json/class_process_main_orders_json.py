import sys
from classes.common.class_db import DB

class ProcessMainOrdersJson(object):
    def __init__(self, configData, logObject):
        self.configData = configData
        self.logObject = logObject


    def execute(self):
        try:
            self.logObject.writeInfo('ProcessOrdersJSON START')
            # Connect to Postgres DB
            self.logObject.writeInfo('ProcessOrdersJSON -- Connecting to Database...')
            dbObject = DB(self.logObject, 'postgresql')
            cursorDB, metaDB = dbObject.connect(self.configData['postgres']['db'],
                                        self.configData['postgres']['user'],
                                        self.configData['postgres']['password'],
                                        self.configData['postgres']['host'],
                                        self.configData['postgres']['port'])

            self.logObject.writeInfo('ProcessOrdersJSON -- Connected to Database.')

            # TODO: JEyson activities


            # Read orders directory
            orders_dir = self.configData['orders']['dir']
            self.logObject.writeInfo('ProcessOrdersJSON -- Reading files in Orders directory ' + orders_dir)

            self.logObject.writeInfo('ProcessOrdersJSON -- Orders directory was read.')

            # Close the database connection
            dbObject.close()

            self.logObject.writeInfo('ProcessOrdersJSON END')
            return 1
        except:  # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError('ProcessOrdersJSON END. Error: ' + str(value))
            return 0