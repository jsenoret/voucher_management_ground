import sys
import ssl
import urllib2
import json
from bs4 import BeautifulSoup

class ComManager(object):
    def __init__(self, method, configObject):
        self.method = method
        self.configObject = configObject

    def execute(self):
        try:
            if self.method == 'GetMessage':
                return self.__getMessages()
            else:
                return False, False
        except: # catch *all* exceptions
            e = sys.exc_info()[0]
            print("Error: " + str(e))

    def __getMessages(self):
        try:
            # Verify SSL false
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            # Login into Com. Manager
            tokenId = self.__comManagerLogin(ctx)
            # If we get the tokenID proced to call the Com manager to transfer folder
            if tokenId:
                # Call the API to send the message
                sendMessageStatus, messagesData = self.__getMessageAPI(tokenId, ctx)
                # If the API to transfer folder was successfully executed, we have to logout to Com. Manager
                if sendMessageStatus:
                    # Logout to Com. Manager
                    logoutStatus = self.__comManagerLogout(tokenId, ctx)
                    if logoutStatus:
                        status = 'DONE'
                    else:
                        status = 'FAILED'
                else:
                    status = 'FAILED'
            else:
                status = 'FAILED'
            return status, messagesData
        except: # catch *all* exceptions
            e = sys.exc_info()[0]
            print("Error: " + str(e))

    def __comManagerLogin(self, ctx):
        tokenId = ''
        try:
            # Login on the Com.Manager API
            loginUrl = self.configObject["login_url"].replace("$AppID", self.configObject["appID"]).replace("$AppName", self.configObject["appID"] + "Name").replace("$AppLastname", self.configObject["appID"] + "LastName")
            request = urllib2.Request(loginUrl)
            response = urllib2.urlopen(request, context=ctx, timeout=30)
            html = BeautifulSoup(response.read(), 'html.parser')
            tokenId = html.string
            response.close()
            return tokenId
        except urllib2.HTTPError, e:
            print("STATUS FAILED Com Manager Login-> HTTPError: " + str(e.code))
            return tokenId
        except urllib2.URLError, e:
            print("STATUS FAILED Com Manager Login-> HTTPError: " + str(e.code))
            return tokenId
        except: # catch *all* exceptions
            e = sys.exc_info()[0]
            print("STATUS FAILED Com Manager Login-> HTTPError: " + str(e.code))
            return tokenId


    def __comManagerLogout(self, tokenId, ctx):
        result = False
        try:
            # Logout on Satcom API
            request_headers = {
                "Authorization": "AUTH " + tokenId,
            }
            logOutUrl = self.configObject["logout_url"]
            request = urllib2.Request(logOutUrl, headers=request_headers)
            urllib2.urlopen(request, context=ctx, timeout=30)
            result = True
            return result
        except urllib2.HTTPError, e:
            print("STATUS FAILED Com Manager Logout-> HTTPError: " + str(e.code))
            return result
        except urllib2.URLError, e:
            print("STATUS FAILED Com Manager Logout-> HTTPError: " + str(e.code))
            return result
        except: # catch *all* exceptions
            e = sys.exc_info()[0]
            print("STATUS FAILED Com Manager Logout-> HTTPError: " + str(e.code))
            return result

    def __getMessageAPI(self, tokenId, ctx):
        result = False
        try:
            # Get last message
            request_headers = {
                "Authorization": "AUTH " + tokenId,
            }
            url = self.configObject["get_messages_url"]
            request = urllib2.Request(url, headers=request_headers)
            response = urllib2.urlopen(request, context=ctx)
            data = json.load(response)
            #print data
            response.close()
            result = True
            return result, data
        except urllib2.HTTPError, e:
            print("STATUS FAILED Com Manager Get Message-> HTTPError: " + str(e.code))
            return result, False
        except urllib2.URLError, e:
            print("STATUS FAILED Com Manager Get Message-> HTTPError: " + str(e.code))
            return result, False
        except: # catch *all* exceptions
            e = sys.exc_info()[0]
            print("STATUS FAILED Com Manager Get Message-> HTTPError: " + str(e.code))
            return result, False
