import sys
from classes.common.class_timer import Timer

class TimerRegister(object):
    def __init__(self, configObject, logObject):
        self.configObject = configObject
        self.logObject = logObject
        self.configHandler = self.configObject.getDocumentHandler()

    def startTimerRegister(self):
        try:
            # Get cron events from the config_processes.xml file
            uniqueTimerList = self.__getUniqueTimers()
            for key, value in uniqueTimerList.iteritems():
                url = 'http://' + str(self.configHandler['generall']['service']['host']) + ':' \
                      + str(self.configHandler['generall']['service']['port']) \
                      + str(self.configHandler['generall']['service']['serviceName'] + '?' + key)
                timerObject = Timer(key, url, value, self.logObject)
                timerObject.daemon = True   # To kill the thread when the main process is stopped
                timerObject.start()
        except: # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError("Error by register timer: " + str(value))

    def __getUniqueTimers(self):
        try:
            timerList = {}
            for name, timer in self.configHandler['timers'].items():
                if timer['name'] not in timerList:
                    timerList[timer['name']] = timer['interval']
            return timerList
        except: # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError("Error by getting unique timer: " + str(value))
            return 0