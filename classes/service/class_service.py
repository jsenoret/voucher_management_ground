from flask import Flask
from classes.common.class_inititalization import Initialization

class Service(object):
    def __init__(self, configObject, logObject):
        self.configObject = configObject
        self.logObject = logObject
        self.app = Flask(__name__)
        self.__initialization()

    def __initialization(self):
        self.initializationObject = Initialization(self.configObject, self.logObject)
        self.initializationObject.initialization()

    def getApp(self):
        return self.app