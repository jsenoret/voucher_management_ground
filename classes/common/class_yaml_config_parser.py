import yaml
import sys

class ConfigParser(object):
    def __init__(self, configPath):
        self.configPath = configPath
        self.configHandler = ''
        self.__readConfigHandler()

    def getDocumentHandler(self):
        return self.configHandler

    def __readConfigHandler(self):
        try:
            with open(self.configPath, 'r') as f:
                self.configHandler = yaml.load(f)
        except: # catch *all* exceptions
            e = sys.exc_info()[0]
            print("ConfigParser Error: " + str(e))