import sys

class ProcessChatMain(object):
    def __init__(self, configData, logObject):
        self.configData = configData
        self.logObject = logObject

    def execute(self):
        try:
            self.logObject.writeInfo('ProcessChatMain START')

            self.logObject.writeInfo('ProcessChatMain END')
            return 1
        except:  # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError('ProcessChatMain END. Error: ' + str(value))
            return 0