import sys
from classes.common.class_timer_register import TimerRegister

class Initialization(object):
    def __init__(self, configObject, logObject):
        self.configObject = configObject
        self.logObject = logObject

    def initialization(self):
        try:
            self.logObject.writeInfo('Initialization START')
            # Start the process to register the timers
            timerRegisterObject = TimerRegister(self.configObject, self.logObject)
            timerRegisterObject.startTimerRegister()
            self.logObject.writeInfo('Initialization END')
        except:  # catch *all* exceptions
            error = sys.exc_info()[0]
            self.logObject.writeError('Initialization END. Error: ' + str(error))
