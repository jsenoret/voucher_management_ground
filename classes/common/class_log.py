import logging
from logging.handlers import TimedRotatingFileHandler

class Log(object):
    def __init__(self, configObject):
        self.configObject = configObject
        # Get the current logger
        #self.logger = logging.getLogger("GPM")

    def logInit(self):
        # Create a logger
        self.logger = logging.getLogger("GPM")
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(self.configObject['generall']['logpath'])
        #fh = TimedRotatingFileHandler(self.configObject.getLogFilePath(), when="d", interval=1)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        # create formatter and add it to the handlers
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def writeInfo(self, text):
        self.logger.info(text)

    def writeError(self, text):
        self.logger.error(text)