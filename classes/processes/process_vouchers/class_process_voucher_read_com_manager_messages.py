import json
import sys

from classes.common.class_com_manager import ComManager


class ProcessVoucherReadComManagerMessages(object):
    def __init__(self, configData, logObject):
        self.configData = configData
        self.logObject = logObject

    def execute(self):
        try:
            self.logObject.writeInfo('ProcessVoucherReadComManagerMessages START')
            objectComManager = ComManager('GetMessage', self.configData)
            status, messagesData = objectComManager.execute()
            if status:
                if messagesData:
                    messagesList = []
                    for element in messagesData:
                        jsonData = json.loads(element['message'])
                        messagesList.append({'TAIL' : jsonData['flight_no'], 'message' : jsonData['message']})
                        self.logObject.writeInfo('New message from TAIL: ' + jsonData['flight_no'] + ' | message: ' + jsonData['message'])
                else:
                    self.logObject.writeInfo('No new messages')
                self.logObject.writeInfo('ProcessVoucherReadComManagerMessages END')
                return 1
            else:
                self.logObject.writeError('ProcessVoucherReadComManagerMessages END. Error reading messages')
                return 0
        except:  # catch *all* exceptions
            error = sys.exc_info()[0]
            self.logObject.writeError('ProcessVoucherReadComManagerMessages END. Error: ' + str(error))
            return 0