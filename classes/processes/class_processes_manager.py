import sys
import re
import importlib
#from classes.processes.process_vouchers.class_process_voucher_read_com_manager_messages import ProcessVoucherReadComManagerMessages
#from classes.processes.process_orders_json.class_process_main_orders_json import ProcessOrdersJSON
#from classes.processes.process_keep_alive_flight_data.class_process_keep_alive_flight_data import ProcessKeepAliveFlightData


class ProcessesManager(object):
    def __init__(self, configObject, logObject, parameters):
        self.configObject = configObject
        self.logObject = logObject
        self.configHandler = self.configObject.getDocumentHandler()
        self.parameters = parameters

    def execute(self):
        try:
            self.logObject.writeInfo('ProcessesManager START')
            for key, value in self.parameters.items():
                # Log in file the event triggered
                self.logObject.writeInfo('Event triggered: ' + key)
                # Get the processes which the triggered event match with the event
                processesList = self.__getProcessesMatchWithEventTriggered(key)
                result = []
                # Loop to execute the process matched with the triggered event
                for process in processesList:
                    # Get the clas path
                    classPath = 'classes.processes.' + process['name']
                    # Get the class name
                    className = self.__snake2camel(process['name'].split('.')[1]).replace('Class', '')
                    classImport = getattr(importlib.import_module(classPath), className)
                    processObject = classImport(process, self.logObject)
                    statusValue = processObject.execute()
                    result.append({'name': process['name'], 'status': statusValue, 'comment': 'OK'})
            # Print in log the status of the processes
            self.logObject.writeInfo('Processes Manager END. Result: ' + str(result))

        except:  # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError("Processes Manager END. Error: " + str(value))

    def __getProcessesMatchWithEventTriggered(self, eventName):
        try:
            processDataList = []
            for name, process in self.configHandler['processes'].items():
                for name, event in process['triggerEvents'].items():
                    if event['name'] == eventName:
                        processDataList.append(process)
            return processDataList
        except: # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError("Error by getting process macth with triggered event: " + str(value))

    def __snake2camel(self, name):
        return re.sub(r'(?:^|_)([a-z])', lambda x: x.group(1).upper(), name)