import sys
import psycopg2.extras
import sqlalchemy

class DB(object):
    def __init__(self, logObject, type):
        self.logObject = logObject
        self.type = type
        self.connection = 0

    def connect(self, dataBaseName, dataBaseUser, dataBasePwd, dataBaseHost, dataBasePort):
        try:
            url = '{}://{}:{}@{}:{}/{}'
            url = url.format(self.type, dataBaseUser, dataBasePwd, dataBaseHost, dataBasePort, dataBaseName)
            # The return value of create_engine() is our connection object
            engine = sqlalchemy.create_engine(url, client_encoding='utf8')
            # We then bind the connection to MetaData()
            meta = sqlalchemy.MetaData(bind=engine, reflect=True)

            self.connection = engine.connect()
            return engine, meta
        except:  # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError('Connecting to DB. Error: ' + str(value))
            return 0

    def queryExecute(self, query):
        try:
            result = self.connection.execute(query)
            return result
        except:  # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError('Executing query. Error: ' + str(value))
            return 0

    def close(self):
        try:
            if self.connection:
                self.connection.close()
                self.logObject.writeInfo('DB connection successfully closed')
        except:  # catch *all* exceptions
            type, value, traceback = sys.exc_info()
            self.logObject.writeError('Closing the DB connection. Error: ' + str(value))