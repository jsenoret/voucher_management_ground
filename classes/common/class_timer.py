import threading
import requests

class Timer(threading.Thread):
    def __init__(self, name, url, interval, logObject):
        threading.Thread.__init__(self, name=name)
        self._finished = threading.Event()
        self.logObject = logObject
        self.interval = interval
        self.processManagerServiceUrl = url
        self.firstTime = True

    def shutdown(self):
        """Stop this thread"""
        self._finished.set()

    def run(self):
        while 1:
            # Wait 30 seconds the first time because the APM service is not up
            if self.firstTime:
                self._finished.wait(10)
                self.firstTime = False

            if self._finished.isSet(): return
            self.task()

            # sleep for interval or until shutdown
            self._finished.wait(int(self.interval))

    def task(self):
        try:
            #print self._apmUrl
            resp = requests.get(self.processManagerServiceUrl)
            #print resp.json()
            pass
        except requests.exceptions.RequestException as error:
            #self.logObject.writeError('Timer ' + self.interval + '. Error: ' + str(error))
            print('Timer ' + self.interval + '. Error: ' + str(error))